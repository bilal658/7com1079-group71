# https://www.kaggle.com/mylesoneill/world-university-rankings

cwurData <- read.csv ("cwurData.csv")

# Install tidyverse or simply ggplot2 library

install.packages ("tidyverse")

library (tidyverse)

# Histogram for the variables
png("cwur_Histogram.png")
ggplot(cwurData, aes(x=publications) ) + geom_histogram(binwidth=20,color="black",fill="grey") + labs(x = "publications", y="Frequency", title="Histogram of cwur$publications")
dev.off()

# Bar plot for the variables
png("cwurData_Barplot.png")
ggplot(cwurData , aes (x=world_rank , y=publications)) + geom_col(fill ="#0099f9") + ggtitle ("Bar chart of the publications \n vs University Rankings")
dev.off()

# Scatter plot for the variables
png("cwUrData_scatterplot.png")
ggplot(cwurData,aes(x = world_rank, y = publications)) + geom_point() + geom_smooth(method = lm) + ggtitle ("Scatter plot of the publications \n vs University Rankings")
dev.off()

# First component of the statement indicates the data source and maps the components of the data to components of the plot.
# Second component of the statement plots points in the graph.
# Third specifies a geometrical function that adds the regression line.
# Fourth component specifies the title to the plot.

# Correlation test
cor.test (cwurData$world_rank, cwurData$publications, method ="spearman", exact = FALSE)
dev.off()
# Linear Model for the data
cwurData <-lm(publications ~ world_rank)
summary(cwurData)

